type Shop = {
  id: number;
  weight: number;
  rank: number;
};

let shops: Shop[] = [];
for (let i = 0; i < 900; i++) {
  shops.push({
    id: i,
    weight: Math.random() * 10,
    rank: 0,
  });
}
shops.sort((a, b) => -compare(a.weight, b.weight));
shops.forEach((shop, i) => {
  shop.rank = i;
});

function compare(a, b) {
  return a < b ? -1 : a > b ? 1 : 0;
}

console.log({
  top: shops.slice(0, 5),
  bottom: shops.slice(shops.length - 5),
});

let M = 20;
let totalWeight = shops.reduce((acc, c) => acc + c.weight, 0);
let recommendedShops: Shop[] = [];
main: for (;;) {
  for (let shop of shops) {
    if (recommendedShops.length == M) {
      break main;
    }
    if (Math.random() < (shop.weight / totalWeight) * M) {
      recommendedShops.push(shop);
    }
  }
}
console.log({
  M,
  totalWeight,
  m: recommendedShops.length,
  recommendedShops,
});

/**
 * shop 1: 2        | 2/5
 * shop 2: 3        | 3/5
 *
 * total: 5
 *
 */
